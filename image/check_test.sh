#!/bin/bash

set -e

while  [ ! -f  "/test/done" ]; do echo "Waiting for tests to finish"; sleep 1; done

PID=$(cat "${1}")
echo "Tests are done, stopping process with pid ${PID}"
kill "${PID}"